<?php

class Controller_Books extends Controller
{

	public function action_test()
	{
		$books = Model_Books::get_all_book();

		$view = View::forge('welcome/index');
		$view->set('books', $books);
		return $view;
	}

}
