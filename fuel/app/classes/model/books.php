<?php

class Model_Books extends Model_Crud
{
	public static function get_all_book()
	{
		$books = DB::select('*')->from('books')->execute();

		return $books;
	}
}
